from django.db import models

class Finance(models.Model):
    date = models.DateTimeField('date')
    Balance = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    Owed = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    Owe = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    Total = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    NET = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    Total_VAT = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    Virgin_Loan = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    VAT = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    VAT_Due = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    Reclaimable = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)
    Paid = models.DecimalField(max_digits=10, decimal_places=2, default=0.0)


        # def test_finance_sets_net(self):
        #     self.assertEqual(Finance.objects.get(date="2019-03-27").NET, Decimal('12.12'))
        #
        # def test_finance_sets_totalvat(self):
        #     self.assertEqual(Finance.objects.get(date="2019-03-27").Total_VAT, Decimal('12.12'))
        #
        # def test_finance_sets_virginloan(self):
        #     self.assertEqual(Finance.objects.get(date="2019-03-27").Virgin_Loan, Decimal('12.12'))
        #
        # def test_finance_sets_VAT_Due(self):
        #     self.assertEqual(Finance.objects.get(date="2019-03-27").VAT_Due, Decimal('12.12'))
        #
        # def test_finance_sets_reclaimable(self):
        #     self.assertEqual(Finance.objects.get(date="2019-03-27").Reclaimable, Decimal('12.12'))
        #
        # def test_finance_sets_Paid(self):
        #     self.assertEqual(Finance.objects.get(date="2019-03-27").Paid, Decimal('12.12'))
