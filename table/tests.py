from django.test import TestCase
from table.models import Finance
from decimal import Decimal

class FinanceTestCase(TestCase):
    def setUp(self):
        Finance.objects.create(date="2019-03-27", Balance="72.02", Owed="34.25", Owe="12.32", Total="7171.2",
        NET="12.12", Total_VAT="12.12", Virgin_Loan="12.12", VAT_Due="12.12",  VAT="12.12", Reclaimable="12.12",
        Paid="12.12"
        )


#Animal.objects.create(name="cat", sound="meow")


    def test_finance_sets_balance(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").Balance, Decimal('72.02'))

    def test_finance_sets_owed(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").Owed, Decimal('34.25'))

    def test_finance_sets_owe(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").Owe, Decimal('12.32'))

    def test_finance_sets_total(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").Total, Decimal('7171.2'))

    def test_finance_sets_net(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").NET, Decimal('12.12'))

    def test_finance_sets_totalvat(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").Total_VAT, Decimal('12.12'))

    def test_finance_sets_virginloan(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").Virgin_Loan, Decimal('12.12'))

    def test_finance_sets_VAT(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").VAT, Decimal('12.12'))

    def test_finance_sets_VAT_Due(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").VAT_Due, Decimal('12.12'))

    def test_finance_sets_reclaimable(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").Reclaimable, Decimal('12.12'))

    def test_finance_sets_Paid(self):
        self.assertEqual(Finance.objects.get(date="2019-03-27").Paid, Decimal('12.12'))






#def test_animals_can_speak(self):
    #    """Animals that can speak are correctly identified"""
        #lion = Animal.objects.get(name="lion")
    #    cat = Animal.objects.get(name="cat")
        #self.assertEqual(lion.speak(), 'The lion says "roar"')
